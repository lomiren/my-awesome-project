//
//  ContentView.swift
//  MyAwesomeProject
//
//  Created by Lomiren on 8/28/19.
//  Copyright © 2019 Lomiren. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
